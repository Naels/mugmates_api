package com.mugmate.classes;

import java.math.BigDecimal;
import java.util.Date;

public class PurchaseResponse {
    private Long purchase_id;
    private String buyer;
    private Date purchase_date_time;
    private int quantity_bought;
    private BigDecimal total_cost;
    private String coffee_name;

    public PurchaseResponse(Long purchase_id, String buyer, Date purchase_date_time, int quantity_bought, BigDecimal total_cost, String coffee_name) {
        this.purchase_id = purchase_id;
        this.buyer = buyer;
        this.purchase_date_time = purchase_date_time;
        this.quantity_bought = quantity_bought;
        this.total_cost = total_cost;
        this.coffee_name = coffee_name;
    }

    public PurchaseResponse() {
    }

    public Long getPurchase_id() {
        return purchase_id;
    }

    public void setPurchase_id(Long purchase_id) {
        this.purchase_id = purchase_id;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public Date getPurchase_date_time() {
        return purchase_date_time;
    }

    public void setPurchase_date_time(Date purchase_date_time) {
        this.purchase_date_time = purchase_date_time;
    }

    public int getQuantity_bought() {
        return quantity_bought;
    }

    public void setQuantity_bought(int quantity_bought) {
        this.quantity_bought = quantity_bought;
    }

    public BigDecimal getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(BigDecimal total_cost) {
        this.total_cost = total_cost;
    }

    public String getCoffee_name() {
        return coffee_name;
    }

    public void setCoffee_name(String coffee_name) {
        this.coffee_name = coffee_name;
    }
}
