package com.mugmate.classes;

public class CoffeeDrinkRequest {

    private Long groupId;
    private Long userId;
    private Integer quantityConsumed;

    public CoffeeDrinkRequest() {
    }

    public CoffeeDrinkRequest(Long groupId, Long userId, Integer quantityConsumed) {
        this.groupId = groupId;
        this.userId = userId;
        this.quantityConsumed = quantityConsumed;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getQuantityConsumed() {
        return quantityConsumed;
    }

    public void setQuantityConsumed(Integer quantityConsumed) {
        this.quantityConsumed = quantityConsumed;
    }
}
