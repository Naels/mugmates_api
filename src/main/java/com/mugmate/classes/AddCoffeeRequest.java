package com.mugmate.classes;

public class AddCoffeeRequest {
    private Long groupId;
    private String coffeeName;
    private Integer quantity;
    private Double pricePerUnit;
    private Long userId;

    public AddCoffeeRequest() {
    }

    public AddCoffeeRequest(Long groupId, String coffeeName, Integer quantity, Double pricePerUnit, Long userId) {
        this.groupId = groupId;
        this.coffeeName = coffeeName;
        this.quantity = quantity;
        this.pricePerUnit = pricePerUnit;
        this.userId = userId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getCoffeeName() {
        return coffeeName;
    }

    public void setCoffeeName(String coffeeName) {
        this.coffeeName = coffeeName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(Double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}