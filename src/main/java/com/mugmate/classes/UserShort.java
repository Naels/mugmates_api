package com.mugmate.classes;

public class UserShort {

    private Long userId;
    private String username;

    public UserShort(Long userId, String username) {
        this.userId = userId;
        this.username = username;
    }

    public UserShort() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
