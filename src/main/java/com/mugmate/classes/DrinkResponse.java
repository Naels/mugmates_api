package com.mugmate.classes;

import java.util.Date;

public class DrinkResponse {

    private Long consumption_id;
    private Long stock_id;
    private Long user_id;
    private Date consumption_date_time;
    private int quantity_consumed;
    private String username;
    private String coffee_name;


    public DrinkResponse(Long consumption_id, Long stock_id, Long user_id, Date consumption_date_time, int quantity_consumed, String username, String coffee_name) {
        this.consumption_id = consumption_id;
        this.stock_id = stock_id;
        this.user_id = user_id;
        this.consumption_date_time = consumption_date_time;
        this.quantity_consumed = quantity_consumed;
        this.username = username;
        this.coffee_name = coffee_name;
    }

    public DrinkResponse() {
    }

    public Long getConsumption_id() {
        return consumption_id;
    }

    public void setConsumption_id(Long consumption_id) {
        this.consumption_id = consumption_id;
    }

    public Long getStock_id() {
        return stock_id;
    }

    public void setStock_id(Long stock_id) {
        this.stock_id = stock_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Date getConsumption_date_time() {
        return consumption_date_time;
    }

    public void setConsumption_date_time(Date consumption_date_time) {
        this.consumption_date_time = consumption_date_time;
    }

    public int getQuantity_consumed() {
        return quantity_consumed;
    }

    public void setQuantity_consumed(int quantity_consumed) {
        this.quantity_consumed = quantity_consumed;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCoffee_name() {
        return coffee_name;
    }

    public void setCoffee_name(String coffee_name) {
        this.coffee_name = coffee_name;
    }
}

