package com.mugmate.classes;

public class Group {

    private Long group_id;
    private String groupName;
    private String description;

    public Group() {
    }

    public Group(Long groupId, String groupName, String description) {
        this.group_id = groupId;
        this.groupName = groupName;
        this.description = description;
    }

    public Long getGroupId() {
        return group_id;
    }

    public void setGroupId(Long groupId) {
        this.group_id = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
