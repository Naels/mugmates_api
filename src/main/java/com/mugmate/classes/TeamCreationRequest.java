package com.mugmate.classes;

public class TeamCreationRequest {
    private String name;
    private String description;
    private String userList;

    public TeamCreationRequest(String name, String description, String userList) {
        this.name = name;
        this.description = description;
        this.userList = userList;
    }

    public TeamCreationRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserList() {
        return userList;
    }

    public void setUserList(String userList) {
        this.userList = userList;
    }
}
