package com.mugmate.repository;

import com.mugmate.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    // Custom query to fetch all users excluding a given user ID
    @Query(value = "SELECT * FROM user WHERE user_id <> :excludedUserId", nativeQuery = true)
    List<User> findAllByIdNot(Long excludedUserId);

    // Custom query to fetch users based on a list of user IDs
    @Query(value = "SELECT user_id AS userId, username FROM user WHERE user_id IN ?1", nativeQuery = true)
    List<Object[]> findAllByUserIds(String[] userIds);

    // Custom query to find a user by username and password
    User findByUsernameAndPassword(String username, String password);

    // Custom query to check if a username exists
    boolean existsByUsername(String username);

    // Custom query to check if an email exists
    boolean existsByEmail(String email);

    // Custom query to find a user by username
    User findByUsername(String username);

/*    // Custom query to fetch user details along with their associated teams
    @Query("SELECT u.userId AS user_id, u.username, u.email, t.groupId AS group_id, t.groupName AS group_name, t.description " +
            "FROM User u " +
            "LEFT JOIN Team t ON CONCAT(',', t.users, ',') LIKE CONCAT('%,', CAST(u.userId AS string), ',%') " +
            "WHERE u.userId = ?1")
    List<Map<String, Object>> findUserDetailsWithTeamsById(Long userId);*/

    // Custom query to fetch coffee expenses for a user
    @Query(value = "SELECT p.purchase_id AS purchase_id, p.purchase_date_time AS purchase_date_time, c.coffee_name AS coffee_name, " +
            "p.quantity_bought AS quantity_bought, p.total_cost, u.username AS seller_username " +
            "FROM Purchase p " +
            "INNER JOIN CoffeeStock c ON p.stock_id = c.stock_id " +
            "INNER JOIN User u ON p.user_id = u.user_id " +
            "WHERE p.user_id = ?1", nativeQuery = true)
    List<Map<String, Object>> findCoffeeExpensesByUserId(Long userId);

    // Custom query to fetch coffee ownership for a user
    @Query(value = "SELECT co.consumption_id, co.consumption_date_time, c.coffee_name, co.quantity_consumed, u.username AS owner_username " +
            "FROM Consumption co " +
            "INNER JOIN CoffeeStock c ON co.stock_id = c.stock_id " +
            "INNER JOIN User u ON co.user_id = u.user_id " +
            "WHERE co.user_id = ?1", nativeQuery = true)

    List<Map<String, Object>> findCoffeeOwnershipByUserId(Long userId);
}