package com.mugmate.repository;

import com.mugmate.entity.CoffeeStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoffeeStockRepository extends JpaRepository<CoffeeStock, Long> {
}
