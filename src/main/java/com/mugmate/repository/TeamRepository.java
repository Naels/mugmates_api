package com.mugmate.repository;

import com.mugmate.entity.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

    @Query(value = "SELECT group_id, group_name, description FROM team WHERE FIND_IN_SET(?1, users)",nativeQuery = true)
    List<Object[]> findByUsersContaining(Long userId);

    @Query(value = "SELECT USERS FROM team WHERE group_id = ?1", nativeQuery = true)
    String  findUsersByGroupId(Long groupId);

    @Query(value = "SELECT DISTINCT purchase.*, user.username, coffeestock.coffee_name FROM CoffeeStock " +
            "INNER JOIN purchase ON CoffeeStock.group_id = purchase.stock_id " +
            "INNER JOIN user ON purchase.user_id = user.user_id " +
            "WHERE CoffeeStock.group_id = ?1", nativeQuery = true)
    List<Object[]> findPurchasesForTeam(String teamId);

    @Query(value = "SELECT DISTINCT consumption.consumption_id as consumption_id, " +
            "coffeestock.stock_id, " +
            "user.user_id, " +
            "consumption.consumption_date_time, " +
            "consumption.quantity_consumed, " +
            "user.username, " +
            "coffeestock.coffee_name " +
            "FROM Consumption " +
            "INNER JOIN user ON consumption.user_id = user.user_id " +
            "INNER JOIN coffeestock ON consumption.stock_id = coffeestock.stock_id " +
            "WHERE Consumption.stock_id = ?1", nativeQuery = true)
    List<Object[]> findDrinksForTeam(Long teamId);

    @Query(value = "SELECT SUM(quantity) FROM CoffeeStock WHERE group_id = ?1", nativeQuery = true)
    Integer getAvailableStockQuantity(Long groupId);

    @Query(value = "SELECT SUM(quantity_consumed) FROM Consumption WHERE stock_id = ?1", nativeQuery = true)
    Integer getConsumedQuantity(Long groupId);
}