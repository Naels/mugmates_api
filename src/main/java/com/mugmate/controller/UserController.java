package com.mugmate.controller;

import com.mugmate.entity.User;
import com.mugmate.classes.UserShort;
import com.mugmate.repository.TeamRepository;
import com.mugmate.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
public class UserController {
    private final UserRepository userRepository;
    private final TeamRepository teamRepository;

    @Autowired
    public UserController(UserRepository userRepository, TeamRepository teamRepository) {
        this.userRepository = userRepository;
        this.teamRepository = teamRepository;
    }

    @GetMapping("/users/exclude/{user_id}")
    public List<User> getUsersExcludingUserId(@PathVariable("user_id") Long excludedUserId) {
        return userRepository.findAllByIdNot(excludedUserId);
    }

    @GetMapping("/user/usernameById/{user_id}")
    public String getUsernameByUserId(@PathVariable("user_id") Long userId) {
        User user = userRepository.findById(userId).orElse(null);
        return user != null ? user.getUsername() : null;
    }

    @GetMapping("/users/{group_id}")
    public List<UserShort> getUsersByGroupId(@PathVariable("group_id") Long groupId) {
        String users = teamRepository.findUsersByGroupId(groupId);
        if (users != null) {
            String[] userIds = users.split(",");
            List<Object[]> userObjects = userRepository.findAllByUserIds(userIds);

            List<UserShort> userShorts = new ArrayList<>();
            for (Object[] userObject : userObjects) {
                UserShort userShort = new UserShort();
                userShort.setUserId((Long) userObject[0]);
                userShort.setUsername((String) userObject[1]);
                userShorts.add(userShort);
            }

            return userShorts;
        } else {
            return Collections.emptyList();
        }
    }

    // Check if user exists
    @PostMapping("/user/exists")
    public Map<String, Object> checkUserExists(@RequestBody Map<String, String> request) {
        String username = request.get("username");
        String password = request.get("password");

        User user = userRepository.findByUsername(username);
        if (user != null) {
            Map<String, Object> response = new HashMap<>();
            response.put("exists", true);
            response.put("user_id", user.getUserId());
            return response;
        } else {
            return Collections.singletonMap("exists", false);
        }
    }

    @PostMapping("/user/username/exists")
    public Map<String, Object> checkUsernameAndEmailExists(@RequestBody Map<String, String> request) {
        String username = request.get("username");
        String email = request.get("email");
        String password = request.get("password");

        boolean usernameExists = userRepository.existsByUsername(username);
        boolean emailExists = userRepository.existsByEmail(email);

        Map<String, Object> response = new HashMap<>();
        if (usernameExists && emailExists) {
            response.put("exists", true);
            response.put("message", "Username and email already exist");
            response.put("existingField", "both");
        } else if (usernameExists) {
            response.put("exists", true);
            response.put("message", "Username already exists");
            response.put("existingField", "username");
        } else if (emailExists) {
            response.put("exists", true);
            response.put("message", "Email already exists");
            response.put("existingField", "email");
        } else {
            User newUser = new User();
            newUser.setUsername(username);
            newUser.setEmail(email);
            newUser.setPassword(password);
            userRepository.save(newUser);

            response.put("exists", false);
            response.put("message", "User inserted successfully");
            response.put("userId", newUser.getUserId());
        }
        return response;
    }

    @GetMapping("/user/password/{username}")
    public Map<String, Object> getUserPasswordByUsername(@PathVariable("username") String username) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            Map<String, Object> response = new HashMap<>();
            response.put("userId", user.getUserId());
            response.put("password", user.getPassword());
            return response;
        } else {
            return null;
        }
    }

    @GetMapping("/user/{user_id}")
    public Map<String, Object> getUserDetailsById(@PathVariable("user_id") Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            List<Object[]> groups = teamRepository.findByUsersContaining(userId);
            List<Map<String, Object>> teamDetails = groups.stream()
                    .map(groupObject -> {
                        Map<String, Object> teamMap = new HashMap<>();
                        teamMap.put("group_id", groupObject[0]);
                        teamMap.put("group_name", groupObject[1]);
                        teamMap.put("description", groupObject[2]);
                        return teamMap;
                    })
                    .collect(Collectors.toList());

            Map<String, Object> response = new HashMap<>();
            response.put("user_id", userId);
            response.put("username", user.get().getUsername());
            response.put("email", user.get().getEmail());
            response.put("teams", teamDetails);

            return response;
        } else {
            return Collections.singletonMap("error", "User not found");
        }
    }

    @GetMapping("/user/{user_id}/coffee-expenses")
    public List<Map<String, Object>> getCoffeeExpensesByUserId(@PathVariable("user_id") Long userId) {
        return userRepository.findCoffeeExpensesByUserId(userId);
    }

    @GetMapping("/user/{user_id}/coffee-ownership")
    public List<Map<String, Object>> getCoffeeOwnershipByUserId(@PathVariable("user_id") Long userId) {
        return userRepository.findCoffeeOwnershipByUserId(userId);
    }
}