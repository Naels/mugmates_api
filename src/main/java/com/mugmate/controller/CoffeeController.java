package com.mugmate.controller;

import com.mugmate.classes.AddCoffeeRequest;
import com.mugmate.classes.CoffeeDrinkRequest;
import com.mugmate.entity.CoffeeStock;
import com.mugmate.entity.Consumption;
import com.mugmate.entity.Purchase;
import com.mugmate.repository.CoffeeStockRepository;
import com.mugmate.repository.ConsumptionRepository;
import com.mugmate.repository.PurchaseRepository;
import com.mugmate.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

@RestController
@CrossOrigin(origins = "*")
public class CoffeeController {

    private final CoffeeStockRepository coffeeStockRepository;
    private final PurchaseRepository purchaseRepository;
    private TeamRepository teamRepository;
    private ConsumptionRepository consumptionRepository;

    @Autowired
    public CoffeeController(CoffeeStockRepository coffeeStockRepository,
                            PurchaseRepository purchaseRepository,
                            TeamRepository teamRepository,
                            ConsumptionRepository consumptionRepository) {
        this.coffeeStockRepository = coffeeStockRepository;
        this.purchaseRepository = purchaseRepository;
        this.teamRepository = teamRepository;
        this.consumptionRepository = consumptionRepository;
    }

    @PostMapping("/coffee/addstock")
    public ResponseEntity<?> addCoffeeToStock(@RequestBody AddCoffeeRequest request) {
        Long groupId = request.getGroupId();
        String coffeeName = request.getCoffeeName();
        Integer quantity = request.getQuantity();
        Double pricePerUnit = request.getPricePerUnit();
        Long userId = request.getUserId();

        if (groupId == null || coffeeName == null || quantity == null || pricePerUnit == null || userId == null) {
            return ResponseEntity.badRequest().body("Missing required fields");
        }

        try {
            // Insert the coffee into the CoffeeStock table
            CoffeeStock coffeeStock = new CoffeeStock();
            coffeeStock.setGroupId(groupId);
            coffeeStock.setCoffeeName(coffeeName);
            coffeeStock.setQuantity(quantity);
            coffeeStock.setPricePerUnit(BigDecimal.valueOf(pricePerUnit));

            CoffeeStock savedCoffeeStock = coffeeStockRepository.save(coffeeStock);

            // Create a purchase entry for the added coffee
            Purchase purchase = new Purchase();
            purchase.setStockId(groupId);
            purchase.setUserId(userId);
            purchase.setPurchaseDateTime(LocalDateTime.now());
            purchase.setQuantityBought(quantity);
            purchase.setTotalCost(BigDecimal.valueOf(quantity * pricePerUnit));

            purchaseRepository.save(purchase);

            return ResponseEntity.ok(Collections.singletonMap("success", true));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to add coffee to stock");
        }
    }

    @PostMapping("/coffee/drink")
    public ResponseEntity<?> consumeCoffee(@RequestBody CoffeeDrinkRequest request) {
        Long groupId = request.getGroupId();
        Long userId = request.getUserId();
        Integer quantityConsumed = request.getQuantityConsumed();

        if (groupId == null || userId == null || quantityConsumed == null || quantityConsumed <= 0) {
            return ResponseEntity.badRequest().body("Invalid input data");
        }

        try {
            Integer availableQuantity = teamRepository.getAvailableStockQuantity(groupId);
            Integer consumedQuantity = teamRepository.getConsumedQuantity(groupId);

            consumedQuantity = consumedQuantity == null ? 0 : consumedQuantity;

            if (availableQuantity == null || availableQuantity - consumedQuantity - quantityConsumed < 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No coffee available in stock");
            }

            Consumption consumption = new Consumption();
            consumption.setStockId(groupId);
            consumption.setUserId(userId);
            consumption.setConsumptionDateTime(LocalDateTime.now());
            consumption.setQuantityConsumed(quantityConsumed);

            consumptionRepository.save(consumption);

            return ResponseEntity.ok(Collections.singletonMap("success", true));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to consume coffee");
        }
    }
}
