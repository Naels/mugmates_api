package com.mugmate.controller;

import com.mugmate.classes.AddCoffeeRequest;
import com.mugmate.classes.DrinkResponse;
import com.mugmate.classes.PurchaseResponse;
import com.mugmate.entity.CoffeeStock;
import com.mugmate.entity.Purchase;
import com.mugmate.entity.Team;
import com.mugmate.classes.TeamCreationRequest;
import com.mugmate.repository.CoffeeStockRepository;
import com.mugmate.repository.PurchaseRepository;
import com.mugmate.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
public class TeamController {

    private final TeamRepository teamRepository;


    @Autowired
    public TeamController(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @PostMapping("/teams")
    public ResponseEntity<?> createTeam(@RequestBody TeamCreationRequest request) {
        String name = request.getName();
        String description = request.getDescription();
        String userList = request.getUserList();

        if (name == null || description == null || userList == null) {
            return ResponseEntity.badRequest().body("Missing required fields");
        }

        Team team = new Team();
        team.setGroupName(name);
        team.setDescription(description);
        team.setUsers(userList);

        try {
            Team savedTeam = teamRepository.save(team);
            return ResponseEntity.ok(savedTeam.getGroupId());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error");
        }
    }

    @GetMapping("/team/{team_id}/purchases")
    public ResponseEntity<?> getPurchasesForTeam(@PathVariable("team_id") String teamId) {
        try {
            List<Object[]> purchasesData = teamRepository.findPurchasesForTeam(teamId);
            List<PurchaseResponse> purchases = purchasesData.stream()
                    .map(data -> {
                        PurchaseResponse purchaseResponse = new PurchaseResponse();

                        purchaseResponse.setPurchase_id(       (Long)       data[0]);
                        purchaseResponse.setBuyer(             (String)     data[6]);
                        purchaseResponse.setPurchase_date_time((Date)       data[3]);
                        purchaseResponse.setQuantity_bought(   (int)        data[4]);
                        purchaseResponse.setTotal_cost(        (BigDecimal) data[5]);
                        purchaseResponse.setCoffee_name(       (String)     data[7]);

                        return purchaseResponse;
                    })
                    .collect(Collectors.toList());

            return ResponseEntity.ok(purchases);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error");
        }
    }

    @GetMapping("/team/{team_id}/drinks")
    public ResponseEntity<?> getDrinksForTeam(@PathVariable("team_id") Long teamId) {
        try {
            List<Object[]> drinksData = teamRepository.findDrinksForTeam(teamId);
            List<DrinkResponse> drinks = drinksData.stream()
                    .map(data -> {
                        DrinkResponse drinkResponse = new DrinkResponse();
                        drinkResponse.setConsumption_id(          (Long) data[0]);
                        drinkResponse.setStock_id(                (Long) data[1]);
                        drinkResponse.setUser_id(                 (Long) data[2]);
                        drinkResponse.setConsumption_date_time(   (Date) data[3]);
                        drinkResponse.setQuantity_consumed(       (Integer) data[4]);
                        drinkResponse.setUsername(                (String) data[5]);
                        drinkResponse.setCoffee_name(             (String) data[6]);
                        return drinkResponse;
                    })
                    .collect(Collectors.toList());
            System.out.println(drinks);
            return ResponseEntity.ok(drinks);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error");
        }
    }
}